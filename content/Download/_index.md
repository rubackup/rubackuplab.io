+++
weight = 110
title = "Download"
nameInMenu = "Download"
draft = false
aliases = ["/download"]
+++

You can download either the official sources or a pre-compiled package for
RHEL/CentOS/Rocky. In any case the [aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/)
must be present on your system.
<br/>

| Download filename      | sha256 checksum                                                               |
|:----------------------:|:-----------------------------------------------------------------------------:|
| [rubackup-0.2.3.tar.gz](https://github.com/fdupoux/rubackup/releases/download/0.2.3/rubackup-0.2.3.tar.gz) | 8a6817abe60ac68db42e539ab0df6a8729cb4d1b06a3ec055c27bea0e18cff4f |
| [rubackup-0.2.3-1.el7](https://github.com/fdupoux/rubackup/releases/download/0.2.3/rubackup-0.2.3-1.el7.noarch.rpm) | 2cb346659c806a587b71f2c3a94b21fc07a6b58865dc2306053a15cdae2c3c33 |
| [rubackup-0.2.3-1.el8](https://github.com/fdupoux/rubackup/releases/download/0.2.3/rubackup-0.2.3-1.el8.noarch.rpm) | 11d0291062183706e6c2f0f018e1ada0397665e269fe319aa24b1c171f6dda02 |
