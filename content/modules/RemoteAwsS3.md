+++
title = "Module remote-aws-s3"
draft = false
aliases = ["/ModuleRemoteAwsS3"]
+++

## Overview

This module provides support for uploading local backups created by
rubackup to a bucket in [AWS S3](http://aws.amazon.com/s3/) (Simple
Storage Service) in order to have offsite backups. For this module to
work the [aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/)
is required. Read the [installation page](/Installation/) for
more details. S3 is an object store in AWS with a very high level of
resilience and hence a very low risk of having you data lost because
of a corruption or hardware failure. This is a `remote` module which means
it must be combined with a `backup` module in a backup entry. The `backup`
module will produce a backup file locally and this module will then upload
the local backup to S3 according to the schedule policy. The schedules used
for the backup and for the upload can be different. For example you can
create daily backups locally and only upload the backups to S3 on a weekly
basis. This module can also be used with an encryption module if you want
the data send to S3 to be encrypted.

## Configuration

This module requires two types of resources to be defined in the
configuration. It requires an Access Key to be defined in the
`aws_access_keys` section and it requires a bucket to be defined in the
`aws_s3_buckets` section. The bucket definition has a reference to the
access key resource. The access key must have the privilege to perform
the S3 operations for this to work.

## Example

Here is a full example which shows how to use this module in the
configuration. It has been combined with a backup module which creates a
tar archive, and with the GnuPG module to perform encryption.

    ---
    global:
        day_of_week: Fri
        day_of_month: 1
    schedules:
        my_schedule_local:
            daily: 7
            weekly: 5
            monthly: 0
        my_schedule_remote:
            daily: 0
            weekly: 5
            monthly: 24
    aws_access_keys:
        access_key_s3:
            public: 'Your_AWS_Access_Key_Here_PUBLIC'
            secret: 'Your_AWS_Access_Key_Here_SECRET'
    aws_s3_buckets:
        my_bucket:
            bucket: my_s3_bucket_name
            awsregion: eu-west-1
            accesskey: access_key_s3
    entries:
        awstats:
            backup_type: ModuleBackupTarball
            backup_opts:
                includes:
                    - /data/awstats
            backup_schedule: my_schedule_local
            bakfile_dir: /backup/backup-web
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: webbkp-awstats-data
            remote_type: ModuleRemoteAwsS3
            remote_opts:
                s3_bucket: my_bucket
            remote_schedule: my_schedule_remote
            encrypt_type: ModuleEncryptGnupg
            encrypt_opts:
                recipient: 'John Smith'
