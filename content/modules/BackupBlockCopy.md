+++
title = "Module Backup-Block-Copy"
draft = false
aliases = ["/ModuleBackupBlockCopy"]
+++

## Overview

This module provides support for creating a low-level copy of a block
device to a compressed flat file. This can be very useful when a block
device is used as a virtual disk for a virtual machine. You must be sure
the block device won't receive any modification during the time of the
backup for it to be consistent. It means the block device must either
not be used (not mounted), or you must work on a snapshot of such a
device. rubackup can perform a snapshot of LVM logical volumes just
before the backup in order to make consistent backup of block devices
while they are in use.

## Configuration

The most important argument is `block_device` and it must point to the
block device for which you want to create a backup such as `/dev/sda1` or
`/dev/VolGroup00/LogVol00` if you are using
[LVM](https://www.system-rescue.org/lvm-guide-en/). If you want to
make backup of LVM Logical-Volumes then you can also get rubackup to
create a snapshot of it using the two optional arguments:
`snapshot_type` and `snapshot_size`. The `snapshot_type` must be set to
`lvm` as this is the only type of snapshot which is supported at the
moment. The `snapshot_size` argument corresponds to the amount of space
reserved for the snapshot storage. It must be big enough so all changes
performed during the lifetime of the snapshot can be stored, and it must
be small enough to fit in the LVM Volume Group. The LVM utilities must
be installed for it to work. Naturally the snapshot will automatically
be removed at the end of the backup.

You can provide the name of the compression program to use (such as
`gzip`, `bzip2` or `xz`) the default being `xz` (you must not put the
path). Also you can provide extra arguments to be passed to the
compression program using `compress_opts`. The compression program must
be installed on the system for this module to work. But as these are
very standard programs this should not be a problem.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Sun
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        virtual-machine-001-img:
            backup_schedule: my_schedule
            backup_type: ModuleBackupBlockCopy
            backup_opts:
                compress_prog: xz
                compress_opts:
                    - '-6'
                block_device: /dev/VolGroupKvm/virtual-machine-001
                snapshot_type: lvm
                snapshot_size: 2G
            bakfile_dir: /backup/backup-kvm
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: kvmbkp-virtual-machine-001
        windows-disk-img:
            backup_schedule: my_schedule
            backup_type: ModuleBackupBlockCopy
            backup_opts:
                compress_prog: xz
                compress_opts:
                    - '-8'
                block_device: /dev/sda1
            bakfile_dir: /backup/backup-win
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: windows-disk
