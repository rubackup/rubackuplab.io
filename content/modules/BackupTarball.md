+++
title = "Module Backup-tarball"
draft = false
aliases = ["/ModuleBackupTarball"]
+++

## Overview

This module provides support for creating compressed tar archives. You
can provide one or more items to include in the archive, and you can
also provide an optional list of items to exclude. It is possible to use
the `command_opts` argument to add extra options on the `tar` command
line. You can also provide the name of the compression program to use
(such as `gzip`, `bzip2` or `xz`) the default being `xz` (you must not
put the path). Also you can provide extra arguments to be passed to the
compression program using `compress_opts`. GNU tar and the compression
program corresponding to the compression mode you want to use must be
installed on the system for this module to work. But as these are very
standard programs this should not be a problem.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Mon
        day_of_month: 1
    schedules:
        myschedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        website1-tar:
            enabled: true
            backup_type: ModuleBackupTarball
            backup_opts:
                includes:
                    - /var/www/website1
                    - /etc/httpd/conf.d/website1.conf
                excludes:
                    - '.git'
                    - '*.tmp'
                    - 'cache'
                command_opts:
                    - '--sparse'
                compress_prog: xz
                compress_opts:
                    - '-3'
                    - '--memlimit-compress=80MiB'
            backup_schedule: myschedule
            bakfile_dir: /backup/backup-web
            bakfile_basename: backup-website1-www
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
