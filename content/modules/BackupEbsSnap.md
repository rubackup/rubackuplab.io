+++
title = "Module Backup-Ebs-Snap"
draft = false
aliases = ["/ModuleBackupEbsSnap"]
+++

## Overview

This module provides support for creating snapshots of
[EBS volumes](http://aws.amazon.com/ebs/) in AWS EC2. For this module to
work the [aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/) is required.
Read the [installation page](/Installation/) for more details. EBS Snapshots
are a very powerful and flexible way to make reliable backup of the system and
data of EC2 instances. This module will automatically find all EBS Volumes
which are attached to the current EC2 instances running rubackup, and perform
a snapshot for all of them.

EBS Snapshots are produced in the cloud so this module does not create
any local backup file on the system where it runs. Hence backups cannot
be processed using encryption or uploading modules.

## Configuration

This module requires at least the name of the AWS Region where the EC2
instance is located, and the access keys with enough privileges to
perform the operation. It also support two optional arguments to improve
the consistency of the snapshots: `fsfreeze` and `stopsvc`. You can
provide a list of filesystem mount points in `fsfreeze` if you want
rubackup to freeze these filesystems before the snapshot is taken. This
will flush filesystem transactions and hence make the filesystem
consistent in the snapshot. Naturally all filesystems will be unfrozen
just after the operation.

Also you can provide a list of services that you want to stop using
`stopsvc`. It may not be possible to use this option is you cannot have
any down time. But it you can stop these services during a few seconds
this will help make the application data consistent in the snapshot. It
may be possible to stop services even on production servers if these
services are part of a cluster and the backup do not all run at the same
time.

## Example

Here is a full example which shows how to use this module in the
configuration:
```
  ---
  global:
      day_of_week: Mon
      day_of_month: 1
  schedules:
      myschedule:
          daily: 7
          weekly: 4
          monthly: 12
  aws_access_keys:
      access_key_snap:
          public: 'Your_AWS_Access_Key_Here_PUBLIC'
          secret: 'Your_AWS_Access_Key_Here_SECRET'
  entries:
      myserver-ebs-snap:
          enabled: true
          backup_type: ModuleBackupEbsSnap
          backup_opts:
              awsregion: us-west-2
              accesskey: access_key_snap
              fsfreeze:
                  - /
                  - /boot
                  - /data
              stopsvc:
                  - httpd
                  - postgresql
          bakfile_basename: snap_myserver
          backup_schedule: myschedule
```

## Access key privileges

This operation will fail if your AWS Access Key does not have enough
privileges. Here is an example of a policy you can paste in IAM in order
to get just the right privileges:
```
{
 "Statement": [
   {
     "Sid": "1",
     "Action": [
       "ec2:CreateSnapshot",
       "ec2:CreateTags",
       "ec2:DeleteSnapshot",
       "ec2:DescribeInstances",
       "ec2:DescribeSnapshotAttribute",
       "ec2:DescribeSnapshots",
       "ec2:DescribeSnapshotsAttribute",
       "ec2:DescribeVolumeAttribute",
       "ec2:DescribeVolumeStatus",
       "ec2:DescribeVolumes",
       "ec2:DescribeTags",
       "ec2:ResetSnapshotAttribute"
     ],
     "Effect": "Allow",
     "Resource": [
       "*"
     ],
   }
 ]
}
```
