+++
title = "Module Backup-Fsarchiver"
draft = false
aliases = ["/ModuleBackupFsarchiver"]
+++

## Overview

This module provides support for creating
[fsarchiver](http://www.fsarchiver.org) backups. You must be sure the
filesystems to backup won't receive any modification during the time of
the backup for it to be consistent. It means the block device must
either not be used (not mounted), or you must work on a snapshot of such
a device. rubackup can perform a snapshot of LVM logical volumes just
before the backup in order to make consistent backup of block devices
while they are in use. The fsarchiver program must be installed for it
to work.

## Configuration

The most important argument is `filesystems` which is a list of
filesystems as shown in the example. Each entry of the list must point
to the block device for which you want to create a backup such as
`/dev/sda1` or `/dev/VolGroup00/LogVol00` if you are using
[LVM](https://www.system-rescue.org/lvm-guide-en/).

If you want to make backup of LVM Logical-Volumes then you can also get
rubackup to create a snapshot of it using the two optional arguments:
`snapshot_type` and `snapshot_size`. The `snapshot_type` must be set to
`lvm` as this is the only type of snapshot which is supported at the
moment. The `snapshot_size` argument corresponds to the amount of space
reserved for the snapshot storage. It must be big enough so all changes
performed during the lifetime of the snapshot can be stored, and it must
be small enough to fit in the LVM Volume Group. The LVM utilities must
be installed for it to work. Naturally the snapshot will automatically
be removed at the end of the backup.

You can also set optional arguments such as `description`, a list of
files or directories to exclude in `excludes` and extra command
arguments to pass to fsarchiver in `command_opts`.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Sun
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        sysimg-centos7:
            backup_type: ModuleBackupFsarchiver
            backup_schedule: my_schedule
            backup_opts:
                description: 'Consistent backup of CentOS 7 root file system and home directory'
                excludes:
                    - '/tmp/*'
                    - '/var/tmp/*'
                command_opts:
                    - '-z8'
                    - '-j6'
                filesystems:
                    - block_device: /dev/sda1
                    - block_device: /dev/VolGroup00/root
                      snapshot_type: lvm
                      snapshot_size: 4096M
                    - block_device: /dev/VolGroup00/home
                      snapshot_type: lvm
                      snapshot_size: 1024M
            bakfile_dir: /backup/backup-fsa
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: system-image-centos7
