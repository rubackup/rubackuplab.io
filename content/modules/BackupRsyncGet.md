+++
title = "Module Backup-Rsync-get"
draft = false
aliases = ["/ModuleBackupRsyncGet"]
+++

## Overview

This module provides support for pulling backup for the current day from
a remote server to the local disk using rsync over ssh. The remote files
must have been created using rubackup or use the same naming convention
in terms of basename, date and extension. The SSH public key
authentication must have been configured so rsync can connect over ssh
without any password prompt. You have to provide the rsync connection
details using `remote_host`, `remote_user` and `remote_path`. This
module requires rsync and ssh to be installed.

It is possible to use the `command_opts` argument to add extra options
on the `tar` command line. You can also provide the name of the
compression program to use (such as `gzip`, `bzip2` or `xz`) the default
being `xz` (you must not put the path).

Once the backup has been pulled using rsync, it can be encrypted and/or
uploaded to a remote location using other types of modules.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Mon
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        website1-tar:
            backup_type: ModuleBackupRsyncGet
            backup_schedule: my_schedule
            backup_opts:
                remote_host: 123.123.123.123
                remote_user: myusername
                remote_path: /backup/backup-web
                extension: .tar.xz
                command_opts:
                    - '--bwlimit=2048'
                    - '--no-o'
                    - '--no-g'
            bakfile_dir: /backup/backup-web
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: webbkp-website1-tar
