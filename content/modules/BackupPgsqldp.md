+++
title = "Module Backup-Pgsqldp"
draft = false
aliases = ["/ModuleBackupPgsqldp"]
+++

## Overview

This module provides support for creating PostgreSQL logical dumps. It
is possible to use the `command_opts` argument to add extra options on
the `pg_dump` command line. pg\_dump must be installed on the system for
this module to work.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Sun
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        website1-sql:
            backup_type: ModuleBackupPgsqldp
            backup_opts:
                dbhost: '127.0.0.1'
                dbuser: 'website1'
                dbname: 'website1'
                dbpass: 'SqlPassForWebSite1'
                command_opts:
                    - '--format=custom'
                    - '--compress=8'
                    - '--clean'
            backup_schedule: my_schedule
            bakfile_dir: /backup/backup-web
            bakfile_basename: webbkp-website1-sql
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
