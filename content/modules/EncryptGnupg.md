+++
title = "Module encrypt-gnupg"
draft = false
aliases = ["/ModuleEncryptGnupg"]
+++

## Overview

This module provides support for encrypting local backups created by
rubackup using GnuPG. GnuPG implements public key encryption. This is
very attractive as the key which performs the encryption every time the
backup is created is the public key, and it does not have to be secret.
The private key is used to decrypt your backup. This should happen only
when you want to restore or test a backup. You won't need to access the
private key very often, and hence you can leave the private key in a
very secure place.

This is an `encrypt` module which means it must be combined with a
`backup` module in a backup entry. The `backup` module will produce a
backup file locally and this module will then encrypt the local backup
using GnuPG.

## Security

The public key must be installed on all server running rubackup. But
this is not a problem as the public key does not have to be secret. The
private key must be secret and hence it must be managed very carefully.
For ideal security the key pair should be generated on an offline
computer then copied on DVD or USB supports and stored in a safe deposit
box. As you may have to use the private key time to time to decrypt your
backup, you can also copy the private key to a smart card such as a
[yubikey-neo](https://www.yubico.com/products/yubikey-hardware/yubikey-neo/).
The smart card has to be connected to a USB port whenever you want to
decrypt a file. The hardware smart card performs the decryption and
hence the private key is never visible by the computer even if it had
been compromised. It is also recommended to 
[generate a master key and multiple subkeys](http://blog.josefsson.org/2014/06/23/offline-gnupg-master-key-and-subkeys-on-yubikey-neo-smartcard/)
in order to be able to change the encryption subkey without having to
also change the master key.

## Configuration

This module only requires one configuration argument which provides the
name of the recipient so GnuPG can determine which public key must be
used to perform the encryption. You can use the optional `command_opts`
argument if you have to pass other options to the `gpg` command.

Naturally the GnuPG application must have been installed on the system,
and the keys must have been imported by the root user so GnuPG can find
it. The GnuPG key management is out of the scope of this manual so you
should read the [GnuPG
manual](https://www.gnupg.org/documentation/manuals.html) if necessary.

## Example

Here is a full example which shows how to use this module in the
configuration in order to encrypt a backup previously created by the
tarball module.

    ---
    global:
        day_of_week: Fri
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 5
            monthly: 12
    entries:
        awstats:
            backup_type: ModuleBackupTarball
            backup_opts:
                includes:
                    - /data/awstats
            backup_schedule: my_schedule
            bakfile_dir: /backup/backup-web
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: webbkp-awstats-data
            encrypt_type: ModuleEncryptGnupg
            encrypt_opts:
                recipient: 'John Smith'
                command_opts:
                    - '--utf8-strings'
                    - '-z0'
