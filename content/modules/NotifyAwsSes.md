+++
title = "Module notify-aws-ses"
draft = false
aliases = ["/ModuleNotifyAwsSes"]
+++

## Overview

This module provides support for sending an email using
[AWS SES](http://aws.amazon.com/ses/) (Simple Email Service) containing the
results at the end of the backup. For this module to work the
[aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/) is required. Read the
[installation page](/Installation/) for more details.

## Configuration

This module requires two types of resources to be defined in the
configuration. It requires an Access Key to be defined in the
`aws_access_keys` section and it requires an SES Media to be defined in
the `aws_ses_medias` section. The medias definition has a reference to
the access key resource. The access key must have the privilege to
perform the SES operations for this to work.

## Example

Here is a full example which shows how to use this module in the
configuration.

    ---
    global:
        day_of_week: Mon
        day_of_month: 1
        notify_type: ModuleNotifyAwsSes
        notify_opts:
            ses_media: my_media
        path_extra:
            - /sbin
            - /usr/sbin
    schedules:
        my_schedule:
            daily: 7
            weekly: 5
            monthly: 12
    aws_ses_medias:
        my_media:
            awsregion: us-west-2
            accesskey: access_key_ses
            mailsrc: noreply@company.com
            maildst: rubackup@company.com
    aws_access_keys:
        access_key_ses:
            public: 'Your_AWS_Access_Key_Here_PUBLIC'
            secret: 'Your_AWS_Access_Key_Here_SECRET'
    entries:
        awstats:
            backup_type: ModuleBackupTarball
            backup_opts:
                includes:
                    - /data/awstats
            backup_schedule: my_schedule
            bakfile_dir: /backup/backup-web
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
            bakfile_basename: webbkp-awstats-data
