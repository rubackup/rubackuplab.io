+++
weight = 160
title = "Modules"
nameInMenu = "Modules"
draft = false
aliases = ["/Modules"]
+++

## Overview

Rubackup is based on a modular design. Hence many features can be added
or modified in quite isolated modules which do not impact the rest of
the application. Four modules types have been created, but only the
first type is mandatory as all backup entries need to create a primary
backup, but not all backups need to be encrypted or sent to a remote
location. Each backup entry must use a **backup** module but they do not
have to use the **encrypt** or **remote** ones. The **notify** module
can be used globally in order to send notification at the end of the
run.

## 'backup' modules

Implement the creation and management of a specific type of backup. For
example there is a module which provides support for creating MySQL
dumps. Another one for creating a compressed tar archives. These modules
have to provide the functions to create the backup, list all existing
backups of this types, and delete old versions of these backups. We can
differentiate two type of such modules. There are modules which create a
backup on the local disk. They are very common and can reuse the
functions of the parent class for listing and deleting backups, as the
code is identical in general. There are other types of backup modules
which may not produce backup which are not stored as local files. For
example the module for EBS Snapshot produces snapshots which are objects
in AWS. These types of modules must provide a specific implementation of
all functions including the functions to list and delete previous
backups as the implementation in the base class is not appropriate.

Here is the list of all currently supported 'backup' modules:

* **[ModuleBackupBlockCopy](/modules/BackupBlockCopy/)**:
  creates a compressed low-level copy of a block device (snapshots can
  be created for LVM Volumes)
* **[ModuleBackupEbsSnap](/modules/BackupEbsSnap/)**: creates a
  snapshot of the EBS Volumes attached to the current EC2 instance in AWS
* **[ModuleBackupFsarchiver](/modules/BackupFsarchiver/)**:
  creates a backup of filesystems using [fsarchiver](http://www.fsarchiver.org)
  (snapshots can be created for LVM Volumes)
* **[ModuleBackupMysqldp](/modules/BackupMysqldp/)**: creates a
  compressed logical database dump using mysqldump for MySQL/MariaDB
* **[ModuleBackupPgsqldp](/modules/BackupPgsqldp/)**: creates a
  logical database dump using mysqldump for PostgreSQL
* **[ModuleBackupRsyncGet](/modules/BackupRsyncGet/)**: uses rsync over ssh to
  pull backups from a remote location (must match the same naming convention)
* **[ModuleBackupTarball](/modules/BackupTarball/)**: creates a compressed tar
  archive of files and directories

## 'encrypt' modules

Implement the support for encrypting local backups. A single function
needs to be implemented for that sort of modules.

Here is the list of all currently supported 'encrypt' modules:

-   **[ModuleEncryptGnupg](/modules/EncryptGnupg/)**: Encrypts
    local backups previously created using GnuPG

## 'remote' modules

This modules allow users to copy backups from the local disk to a remote
location such as an S3 bucket in AWS or an FTP server. These modules can
be used to have a second copy of all backups in a remote site. These
modules must provide functions to implement the upload of a local file
to the remote storage, and function to list and delete remote backups.

Here is the list of all currently supported 'remote' modules:

* **[ModuleRemoteAwsS3](/modules/RemoteAwsS3/)**: Uploads and manages local
  backups previously created to the object storage service in AWS.

## 'notify' modules

These modules implement a single function which sends a notification
message. This is used to let the administrator know the status of each
run.

Here is the list of all currently supported 'notify' modules:

* **[ModuleNotifyAwsSes](/modules/NotifyAwsSes/)**: Sends a notification using
  the Simple Email Service provided by AWS

## Parameters

All backup entries have many options in common in their configuration.
But as each module does different things, they all need different
parameters in the configuration. That is why the backup entries provide
secondary level parameters as these parameters are used by a particular
module. For example if you use `backup_type: ModuleBackupTarball` then
you will have to provide options used by this module in `backup_opts`
such as the list of files and directory to include and to exclude in the
archive, and the type of compression to use. Regarding the optional
modules (for encryption and remote backups) you must provide
`encrypt_type` and/or `remote_type` only if you want to use these
modules. In this case you also have to provide the module specific
options in `encrypt_opts` and/or `remote_opts` in the backup entry
configuration.
