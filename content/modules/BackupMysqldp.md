+++
title = "Module Backup-Mysqldp"
draft = false
aliases = ["/ModuleBackupMysqldp"]
+++

## Overview

This module provides support for creating compressed MySQL/MariaDB
logical dumps. It is possible to use the `command_opts` argument to add
extra options on the `mysqldump` command line. You can also provide the
name of the compression program to use (such as `gzip`, `bzip2` or `xz`)
the default being `xz` (you must not put the path). Also you can provide
extra arguments to be passed to the compression program using
`compress_opts`. mysqldump and the compression program corresponding to
the compression mode you want to use must be installed on the system for
this module to work.

## Example

Here is a full example which shows how to use this module in the
configuration:

    ---
    global:
        day_of_week: Sun
        day_of_month: 1
    schedules:
        my_schedule:
            daily: 7
            weekly: 4
            monthly: 12
    entries:
        website1-sql:
            backup_type: ModuleBackupMysqldp
            backup_opts:
                dbhost: '127.0.0.1'
                dbuser: 'website1'
                dbname: 'website1'
                dbpass: 'SqlPassForWebSite1'
                command_opts:
                    - '--add-drop-database'
                    - '--add-drop-table'
                compress_prog: xz
                compress_opts:
                    - '-6'
                    - '--memlimit-compress=80MiB'
            backup_schedule: my_schedule
            bakfile_dir: /backup/backup-web
            bakfile_basename: webbkp-website1-sql
            bakfile_owner: root
            bakfile_group: root
            bakfile_mode: 0600
