+++
weight = 150
title = "Quick Start"
nameInMenu = "QuickStart"
draft = false
aliases = ["/quickstart"]
+++

Using rubackup should be quite simple. Here is what you have to do:

* Make sure you have the dependencies you need (ruby-1.9 or more recent, the
  [aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/) is required if you want
  to use the AWS features, dependency commands used for backups)
* [Install](/Installation/) rubackup on your linux system (you can either use a
  package or copy the ruby source files manually)
* Understand what rubackup [modules](/modules/) are and how they can be used
* Understand how the rubackup yaml [configuration](/Configuration/) works and
  write configuration files for your use cases
* Execute rubackup manually and verify the command outputs
* Create a cronjob to run rubackup once a day automatically for you
