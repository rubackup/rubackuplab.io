+++
weight = 100
title = "rubackup Homepage"
nameInMenu = "Homepage"
draft = false
aliases = ["/Main_Page"]
+++

## About

rubackup is a ruby based **unified backup application** for Linux. It
provides generic features so quite **different backups can be created with
a single program**. Hence you only have to manage a single backup program
with different configurations. It also provides simpler and more
**consistent backups** as they are all named, monitored and rotated the same
way. This is free software. It comes with no warranty. It has been
released under the terms of the GNU General Public License version 2.

## Features

-   Create local backups such as a tar archives, MySQL / PostgreSQL
    logical dump with a checksum file
-   Rotate backups using schedules with a daily/weekly/monthly
    granularity
-   Create and manage [EBS Snapshots](/modules/BackupEbsSnap/) of
    all EBS Volumes attached to EC2 instances
-   Create compressed [copy of a block device](/modules/BackupBlockCopy/)
    (LVM Snapshot can be created)
-   Create [fsarchiver backups](/modules/BackupFsarchiver/) of
    filesystems (LVM Snapshot can be created)
-   [Encrypt backups](/modules/EncryptGnupg/) created locally
    using GnuPG public keys
-   [Upload and manage backups in S3 buckets](/modules/RemoteAwsS3/) in AWS
    in order to have off-site copies
-   Send email notification using AWS SES at the end of each execution

## Scheduling

Backup scheduling policies can be specified as a number of days, weeks,
and months. Different schedules can be used for different things. For
example it is possible to keep 7 daily backups + 5 weekly backups + 12
monthly backups for a particular type of data, and to only upload weekly
and monthly backups to a remote backup area such as AWS S3, in order to
save some bandwidth.

## Encryption

There is an encryption module which adds support for GnuPG encryption.
The very good thing about it is this is using public/private keys. Hence
the secret key is not required for encrypting backups. The public key
which is used for encryption can be copied on many servers without
causing any security issue.
