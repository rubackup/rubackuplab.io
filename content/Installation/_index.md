+++
weight = 130
title = "Installation"
nameInMenu = "Installation"
draft = false
aliases = ["/installation"]
+++

## Overview

Installing rubackup is quite simple: all you have to do is to copy the
ruby files from the official tar archive to a specific location on your
disk. You can also use a packaged version of it such as an RPM package
for CentOS. rubackup is a stateless application hence it does not
require any database to store the state of its backups. Instead it
checks directly which backup files can be found on the disk or other
types of storage.

## Requirements

This is a ruby based application, hence it requires ruby. It requires
ruby-1.9 or any newer version. This program also provides optional AWS
features and hence the [aws-sdk rubygem](https://rubygems.org/gems/aws-sdk/)
must be present on your system if you plan to use the AWS features. This
ruby gem is not required if you do not use the AWS features of rubackup.
This gem can be installed using the gem command:
`gem install aws-sdk`

rubackup requires programs to be installed on the system if they are
needed to perform the backup. For example it requires programs such as
mysqldump to perform MySQL logical dumps, fsarchiver to make fsarchiver
backups, tar to perform a tarball and GnuPG to perform encryption. Also
the compression programs such as gzip and xz must be installed depending
on the compression formats that will be produced.

## Files

You can install rubackup files anywhere you want. Here is an example of
the files you will get if you install it in /opt/rubackup:

    /opt/rubackup/lib/core/bakfile.rb
    /opt/rubackup/lib/core/config.rb
    /opt/rubackup/lib/core/core.rb
    /opt/rubackup/lib/core/exit-status.rb
    /opt/rubackup/lib/core/modules.rb
    /opt/rubackup/lib/core/validators.rb
    /opt/rubackup/lib/modules/backup-blockcopy.rb
    /opt/rubackup/lib/modules/backup-ebssnap.rb
    /opt/rubackup/lib/modules/backup-fsarchiver.rb
    /opt/rubackup/lib/modules/backup-generic.rb
    /opt/rubackup/lib/modules/backup-mysqldp.rb
    /opt/rubackup/lib/modules/backup-pgsqldp.rb
    /opt/rubackup/lib/modules/backup-rsyncget.rb
    /opt/rubackup/lib/modules/backup-tarball.rb
    /opt/rubackup/lib/modules/encrypt-generic.rb
    /opt/rubackup/lib/modules/encrypt-gnupg.rb
    /opt/rubackup/lib/modules/notify-aws-ses.rb
    /opt/rubackup/lib/modules/notify-generic.rb
    /opt/rubackup/lib/modules/remote-aws-s3.rb
    /opt/rubackup/lib/modules/remote-generic.rb
    /opt/rubackup/lib/utils/checksums.rb
    /opt/rubackup/lib/utils/lvm-snapshots.rb
    /opt/rubackup/lib/utils/output.rb
    /opt/rubackup/lib/utils/scheduling.rb
    /opt/rubackup/lib/utils/services.rb
    /opt/rubackup/lib/utils/utilities.rb
    /opt/rubackup/rubackup.rb

## Cronjob

You can make it run at 1am every night by creating a file such as
**/etc/cron.d/rubackup** with the line below in it. This will produce
two log files: the first one will redirect the standard output/error
streams to */var/log/rubackup/rubackup.log* with a medium level of
details for general use. It will also create a more detailed log file
corresponding to a verbosity level 4 for debugging and this will go into
the following file: */var/log/rubackup/rubackup.dbg*. Don't forget to
create */var/log/rubackup* using mkdir first.

    0 1 * * * root /opt/rubackup/rubackup.rb --logfile /var/log/rubackup/rubackup.dbg --loglevel 4 --outlevel 3 >> /var/log/rubackup/rubackup.log 2>&1

Also you can get these two files rotated on a daily basis using
logrotate by creating **/etc/logrotate.d/rubackup** with the following
contents:

    /var/log/rubackup/*log
    /var/log/rubackup/*dbg
    {
      daily
      dateext
      rotate 30
      nocompress
    }

This will produce daily log files like this:

    /var/log/rubackup/rubackup.dbg
    /var/log/rubackup/rubackup.dbg-20150120
    /var/log/rubackup/rubackup.dbg-20150121
    /var/log/rubackup/rubackup.dbg-20150122
    /var/log/rubackup/rubackup.dbg-20150123
    /var/log/rubackup/rubackup.log
    /var/log/rubackup/rubackup.log-20150120
    /var/log/rubackup/rubackup.log-20150121
    /var/log/rubackup/rubackup.log-20150122
    /var/log/rubackup/rubackup.log-20150123
